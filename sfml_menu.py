from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfml_button import Button
from sfml_notify import notify
import os
import time

class Menu:
    def __init__(self, pos, win_size):
        self.position = pos
        self.win_size = win_size
        self.focus = False
        self.menu = {}
        self.active_graph = ""
        self.active_fields = []

    def add_graph(self, name, size, text=False, color=Color.WHITE, picture=""):
        pos = list(self.position)[:]
        if text:
            title = name
        else:
            title = ""
        for j, i in self.menu.items():
            pos[0] += i["btn"].get_size()[0]
        pos[0] += 2
        if os.path.isfile("pic/" + picture):
            self.menu[name] = {"btn": Button(pos, size, self.win_size, color=color, picture=picture), "fields": {}}
        else:
            self.menu[name] = {"btn": Button(pos, size, self.win_size, text=title, color=color, picture=picture), "fields": {}}

    def add_field(self, graph, name, text=False, color=Color.WHITE, picture=""):
        pos = self.menu[graph]["btn"].get_position()
        size = self.menu[graph]["btn"].get_size()
        try:
            fields = self.menu[graph]["fields"]
            x, y = fields[list(fields.keys())[-1]]["btn"].get_position()[0], fields[list(fields.keys())[-1]]["btn"].get_position()[1] + size[1]
        except IndexError:
            x, y = pos[0], pos[1] + size[1]
        pos = (x, y)
        if text:
            title = name
        else:
            title = ""
        if os.path.isfile("pic/menu/" + picture):
            picture = "menu/" + picture
            self.menu[graph]["fields"][name] = {"btn": Button(pos, size, self.win_size, text=title, color=color, picture=picture, mode=0), "name": name}
        else:
            self.menu[graph]["fields"][name] = {"btn": Button(pos, size, self.win_size, text=title, color=color, mode=0), "name": name}

    def del_graph(self, name):
        del self.menu[name]

    def del_field(self, graph, name):
        self.menu[name].remove(name)
        
    def draw(self, window, events):
        for key, i in self.menu.items():
            i["btn"].get_focus(window, events)
            i["btn"].draw(window, events)
            if self.active_graph == key and self.focus:
                for name, field in i["fields"].items():
                    field["btn"].get_focus(window, events)
                    field["btn"].draw(window, events)

    def get_focus(self, window, events):
        self.event_check(window, events)
        self.draw(window, events)
        return self.focus
        
    def event_check(self, window, events):
        mouse_x, mouse_y = Mouse.get_position(window)
        for key, i in self.menu.items():
                if i["btn"].get_focus(window, events):
                    self.active_fields = [i, key]
                    self.active_graph = key
                    time.sleep(0.01)
                    self.focus = True
                    return True
                if self.focus and key == self.active_graph:
                    for k, j in i["fields"].items():
                        if j["btn"].get_focus(window, events):
                            self.active_fields = [i, key]
                            self.active_graph = key
                            self.focus = True
                            return True
                for i in events:
                    if i.type == Event.MOUSE_BUTTON_PRESSED:
                        self.focus = False
                        self.active_graph = ""
                
    def focus_event(self, window, events):
        mouse_x, mouse_y = Mouse.get_position(window)
        pressed = False
        key = self.active_fields[1]
        fields = self.active_fields[0]["fields"]
        elem = self.active_fields[0]
        time.sleep(0.01)
        if Mouse.is_button_pressed(0):
            if elem["btn"].get_focus(window, events):
                pressed = True
            for name, field in fields.items():
                if field["btn"].get_focus(window, events):
                    pressed = True
                    return field["name"]
            if not pressed:
                self.focus = False
                self.active_graph = ""
                elem["btn"].get_focus(window, events)

    def set_color(self, color):
        for k, i in self.menu.items():
            i["btn"].set_color(color)
            for h, j in i["fields"].items():
                j["btn"].set_color(color)
        
                                

