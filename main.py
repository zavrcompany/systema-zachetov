#!/usr/bin/env python3
from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfmlWidgets import *
from sfml_file_manager import *
from get_settings import Settings
import qrcode
import random
import os
import json
import socket
import time
from threading import Thread
from queue import Queue
from keygen import *
from coder import *
import asyncio
import subprocess
from ftplib import FTP
import ftplib
from transliterate import translit
import zipfile
import socket
from pprint import pprint

color_theme_0 = Color(0, 0, 0)
color_theme_1 = Color(200, 200, 200)

def check_network():
    try:
        subprocess.check_output("ping yandex.ru -c 1".split())  
        return True
    except subprocess.CalledProcessError:
        return False

def login_interface(window, win_size, color_theme_0):
    line_1_2 = RectangleShape([win_size.x / 5, win_size.y / 2])
    line_1_2.position = win_size.x / 2 - win_size.x / 10, win_size.y / 2 - win_size.y / 4

    line_1_2.fill_color = color_theme_0
    
    window.draw(line_1_2)
    
def interface(window, win_size, color_theme_0, menu_color):
    line_1_2 = RectangleShape([3, win_size.y])
    line_1_2.position = win_size.x / 10, 0
    line_2_3 = RectangleShape([3, win_size.y])
    line_2_3.position = win_size.x / 2, 0
    line_menu = RectangleShape([win_size.x, 25])
    line_3_4 = RectangleShape([win_size.x / 2, 3])
    line_3_4.position = win_size.x / 2, 25 + win_size.y - win_size.y / 5
    line_3_5 = RectangleShape([3, win_size.y - win_size.y / 5])
    line_3_5.position = win_size.x - win_size.x / 20, 25

    line_message_1 = RectangleShape([win_size.x / 2 - 120, 1])
    line_message_1.position = win_size.x / 2 + 15, 50 + win_size.y - win_size.y / 5 - 10
    line_message_2 = RectangleShape([win_size.x / 2 - 120, 1])
    line_message_2.position = win_size.x / 2 + 15, 50 + win_size.y - win_size.y / 5 + (win_size.y / 2 - win_size.y / 50) / 4 + 10
    line_message_3 = RectangleShape([1, (win_size.y / 2 - win_size.y / 50) / 4 + 20])
    line_message_3.position = win_size.x / 2 + 15, 40 + win_size.y - win_size.y / 5
    line_message_4 = RectangleShape([1, (win_size.y / 2 - win_size.y / 50) / 4 + 20])
    line_message_4.position = win_size.x / 2 + 15 + win_size.x / 2 - 120, 40 + win_size.y - win_size.y / 5

    line_1_2.fill_color = color_theme_0
    line_2_3.fill_color = color_theme_0
    line_3_4.fill_color = color_theme_0
    line_3_5.fill_color = color_theme_0
    line_menu.fill_color = menu_color
    line_message_1.fill_color = color_theme_0
    line_message_2.fill_color = color_theme_0
    line_message_3.fill_color = color_theme_0
    line_message_4.fill_color = color_theme_0
    
    
    window.draw(line_1_2)
    window.draw(line_2_3)
    window.draw(line_menu)
    window.draw(line_3_4)
    window.draw(line_3_5)
    window.draw(line_message_1)
    window.draw(line_message_2)
    window.draw(line_message_3)
    window.draw(line_message_4)

def reciver(sock):
    while True:
        ans = sock.recv(1024).decode()
        if ans:
            print(ans)
            
def init():
    account = {}
    settings = Settings()
    host = (settings.get_host(), settings.get_port())
    window = RenderWindow(VideoMode.get_fullscreen_modes()[0], "Вход")
    icon = Image.from_file("pic/icon.png")
    window.set_icon(1920, 1920, bytes(icon.pixels))
    win_size = window.size
    color_theme_0 = Color(0, 0, 0)
    color_theme_1 = Color(200, 200, 200)
##    color_theme_0, color_theme_1 = color_theme_1, color_theme_0
    login = TextEdit([win_size.x / 2 - win_size.x / 10 + win_size.x / 100, win_size.y / 2 - win_size.y / 4 + win_size.y / 100],
                     [win_size.x / 5 - win_size.x / 50, (win_size.y / 2 - win_size.y / 50) / 8],
                     win_size,
                     text="",
                     reserv_text="Введите логин:",
                     color=color_theme_1)
    
    password = TextEdit([win_size.x / 2 - win_size.x / 10 + win_size.x / 100, win_size.y / 2 - win_size.y / 4 + win_size.y / 50 + ((win_size.y / 2 - win_size.y / 50) / 8)],
                     [win_size.x / 5 - win_size.x / 50, (win_size.y / 2 - win_size.y / 50) / 8],
                     win_size,
                     text="",
                     reserv_text="Введите пароль:",
                     color=color_theme_1,
                     type="password")

    confirm = Button([win_size.x / 2 - win_size.x / 10 + win_size.x / 100, win_size.y / 2 - win_size.y / 4 + win_size.y / 25 + ((win_size.y / 2 - win_size.y / 50) / 8) * 2],
                     [win_size.x / 5 - win_size.x / 50, (win_size.y / 2 - win_size.y / 50) / 8],
                     win_size,
                     text="Войти",
                     color=Color(0, 127, 0))
    confirm.text.color = Color.BLACK

    activable_elements = [login, password]
    
    background = Sprite(Texture.from_file("pic/background_koder.png"))
    
    img = qrcode.make(f"a{settings.get_host()}:25545/check_qrcode?code=" + open("number").read()[:7])
    img.save("tmp.png")
    qr_code = RectangleShape([win_size.x / 5 - win_size.x / 10, win_size.x / 5 - win_size.x / 10])
    qr_code.texture = Texture.from_file("tmp.png")
    qr_code.position = win_size.x / 2 - qr_code.size.x / 2, win_size.y / 2 - qr_code.size.y / 2 + win_size.y / 100 + ((win_size.y / 2 - win_size.y / 50) / 8) * 2

    opened = False
    
    while window.is_open:
        flag = False
        window.clear(color_theme_1)
        
        window.draw(background)
        events = []
        for i in window.events:
            events.append(i)
            
        for event in events:
            if event.type == Event.CLOSED:
                window.close()
            if event.type == Event.KEY_PRESSED:
                if Keyboard.is_key_pressed(Keyboard.TAB):
                    flag = True
        login_interface(window, win_size, color_theme_0)
        if window.has_focus():
            elem = get_focus(activable_elements, window, events)
            if flag:
                if id(elem) == id(activable_elements[0]):
                    elem = activable_elements[1]
                    activable_elements[0].editing = False
                    activable_elements[1].editing = True
            if elem:
                res = elem.focus_event(window, events)
                if res:
                    print(res)

            try:
                if confirm.get_focus(window, events) or Keyboard.is_key_pressed(Keyboard.RETURN):
                    if login.main_text and password.main_text:
                        sock_send = socket.socket()
                        sock_send.connect(host)
                        sock_send.sendall(f"com://connect {encoding(login.main_text, open('number').read()).encode('unicode_escape').decode()}&&{encoding(password.main_text, open('number').read())}".encode())
                        ans = sock_send.recv(1024).decode()
                        if ans == "ok":
                            new_key = str(gen(int(open("number").read())))
                            with open("number", "w") as f:
                                f.write(new_key)
                            opened = True
                            window.close()
                            ans = sock_send.recv(1024).decode().split()
                            account["type"] = ans[0]
                            account["class"] = ans[1]
                            account["login"] = login.main_text
                            sock_chat_recv = socket.socket()
                            sock_chat_recv.connect(host)
                            # sock_chat_recv.send(f"com://init_chat {sock_send.getsockname()[1]}".encode())
                            # Thread(target=reciver, args=(sock_chat_recv,)).start()
                        elif ans == "incorrect":
                            notify("Система Зачётов", "Вход", "Вы ввели некорретные пароль или логин!", os.getcwd() + "/pic/icon.png")
                            password.main_text = ""
                            password.text.string = password.main_text
                        sock_send.close()
                    else:
                        notify("Система Зачётов", "Вход", "Вы заполнили не все поля!", os.getcwd() + "/pic/icon.png")
            except ConnectionRefusedError:
                if check_network():
                    notify("Система Зачётов", "Ошибка", "Сервер временно недоступен", os.getcwd() + "/pic/icon.png")
                else:
                    notify("Система Зачётов", "Ошибка", "Проверьте подключение к интернету", os.getcwd() + "/pic/icon.png")
        password.draw(window, events)
        login.draw(window, events)
        confirm.draw(window, events)
##        window.draw(qr_code)
        window.display()
    if opened:# or queue.get()[0]:
        return main(account)
    return 0

def reciver(sock_recv):
    while True:
        try:
            data = sock_recv.recv(1024)
            if data.decode() == "ping@!#":
                pass
            else:
                print(data.decode())
                user = data.decode().split(":%:")
                if len(user) >= 2:
                    with open(his_path + "chat_" + user[0] + ".hs", mode="a") as f:
                        f.write(time.strftime("%H:%M:%S", time.localtime()) + ":&" + ":%:".join(data.decode().split(":%:")[1:]) + "\n")
        except Exception:
            return

def sender(sock_send, peoples, message):
    sock_send.send(f"[TO:{' '.join(peoples)}]&:&{message}".encode())
    for i in peoples:
        with open(his_path + "chat_" + i + ".hs", mode="a") as f:
            f.write("$IAM$" + time.strftime("%H:%M:%S", time.localtime()) + ":&" + message + "\n")
        
def main(account):
    global color_theme_0
    global color_theme_1
    title = "Система зачётов"

    fm = FileManager()

    settings = Settings()
    chat_host = (settings.get_chat_host(), settings.get_chat_port())
    ftp_host = settings.get_ftp_host()
    ftp_port = settings.get_ftp_port()

    # Initialisation chat
    try:
        ftp = FTP()
        ftp.encoding = "UTF-8"
        ftp.connect(ftp_host, ftp_port)
        ftp.login(user=f"u{account['class']}", passwd=str(account["class"]))
        ftp.cwd(account["type"])
        ftp.cwd(account["class"])
        filenames = ftp.nlst()
        data = ftp.retrlines('LIST')
        for i in filenames:
            with open(dir_path + "/" + i, 'wb') as f:
                ftp.retrbinary('RETR ' + i, f.write)
    except ConnectionRefusedError:
        notify(title, "Ошибка", "Не возможно получить доступ к серверу!", os.getcwd() + '/pic/icon.png')
##        os.popen(f"zenity --error --title='{title}' --window-icon='{os.getcwd() + '/pic/icon.png'}' --text='Не возможно получить доступ к серверу'")
##        return 540
    except ftplib.error_perm:
        notify(title, "Ошибка", "Не возможно получить доступ к серверу!", os.getcwd() + '/pic/icon.png')
##        os.system(f"zenity --error --title='{title}' --window-icon='{os.getcwd() + '/pic/icon.png'}' --text='Не возможно получить доступ к серверу'")
##        return 550
    # End init

    # Initialisation chat
    try:
        s_nick = "send" + "&" + account["login"]
        r_nick = "recv" + "&" + account["login"]
        sock_recv = socket.socket()
        sock_recv.connect(chat_host)
        sock_recv.send(r_nick.encode())
        Thread(target=reciver, args=(sock_recv,)).start()

        sock_send = socket.socket()
        sock_send.connect(chat_host)
        sock_send.send(s_nick.encode())
        data = sock_send.recv(1024)
        if data.decode() == "Ok":
            pass
        else:
            notify(title, "Ошибка", "Не удалось подключиться к серверу сообщений", os.getcwd() + "/pic/icon.png")
            sock_send.close()
            sock_recv.close()
    except ConnectionRefusedError:
        sock_send = ''
        sock_recv = ''
        notify(title, "Ошибка", "Не удалось подключиться к серверу сообщений", os.getcwd() + "/pic/icon.png")
    # End init

    print(account)
    window = RenderWindow(VideoMode.get_fullscreen_modes()[0], title)

    color_theme_0 = Color(0, 0, 0)
    icon_theme_0 = {"description": "description_2.jpg",
                    "code_viewer": "code_2.jpg",
                    "chat_viewer": "chat_2.jpg",
                    "download": "download_2.jpg",
                    "upload": "upload_2.jpg"}
    color_theme_1 = Color(200, 200, 200)
    icon_theme_1 = {"description": "description.jpg",
                    "code_viewer": "code.jpg",
                    "chat_viewer": "chat.jpg",
                    "download": "download.jpg",
                    "upload": "upload.jpg"}

    win_size = window.size
    avatar = Label([win_size.x / 50, 25 + win_size.y / 50],
                   [win_size.x / 10 - win_size.x / 25, (win_size.x / 10 - win_size.x / 25) / 3 * 4],
                   picture="avatar.jpg")
    username = Label([win_size.x / 50, 25 + win_size.y / 50 * 2 + avatar.size.y],
                     [win_size.x / 10 - win_size.x / 25, win_size.y / 25],
                     text="Прогер\nАлексеевич",
                     text_pos="L",
                     font_size=18,
                     color=color_theme_1)
    class_num = Label([win_size.x / 50, 25 + win_size.y / 50 * 2 + avatar.size.y + username.size.y],
                      [win_size.x / 10 - win_size.x / 25, win_size.y / 25],
                      text=f"Класс: {account['class']} б",
                      text_pos="L",
                      color=color_theme_1)
    
    description = Label([win_size.x / 2 + win_size.x / 40, 25 + win_size.y / 40],
                        [win_size.x / 2 - win_size.x / 10, win_size.y - win_size.y / 20 - win_size.y / 5],
                        text="",
                        text_pos="L",
                        text_v_pos="U",
                        color=color_theme_1,
                        font_size=20)

    code_viewer = Label([win_size.x / 2 + win_size.x / 40, 25 + win_size.y / 40],
                        [win_size.x / 2 - win_size.x / 10, win_size.y - win_size.y / 20 - win_size.y / 5],
                        text="",
                        text_pos="L",
                        text_v_pos="U",
                        color=color_theme_1,
                        font_size=20)

    chat_viewer = ScrolledArea([win_size.x / 2 + win_size.x / 40, 25 + win_size.y / 40],
                               [win_size.x / 2 - win_size.x / 12, win_size.y - win_size.y / 20 - win_size.y / 5],
                               win_size,
                               text_pos="L",
                               text_v_pos="U",
                               color=color_theme_1)

    description_btn = Button([win_size.x / 20 * 19 + win_size.x / 200, 25 + win_size.x / 200],
                             [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                             win_size,
                             text="description",
                             picture=icon_theme_1["description"],
                             font_size=0)
    
    code_viewer_btn = Button([win_size.x / 20 * 19 + win_size.x / 200, 25 + win_size.x / 200 * 2 + description_btn.size.y],
                             [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                             win_size,
                             text="code_viewer",
                             picture=icon_theme_1["code_viewer"],
                             font_size=0)
    
    chat_viewer_btn = Button([win_size.x / 20 * 19 + win_size.x / 200, 25 + win_size.x / 200 * 3 + description_btn.size.y + code_viewer_btn.size.y],
                             [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                             win_size,
                             text="chat_viewer",
                             picture=icon_theme_1["chat_viewer"],
                             font_size=0)

    upload_btn = Button([win_size.x / 20 * 19 + win_size.x / 200, win_size.x / 3],
                        [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                        win_size,
                        text="upload",
                        picture=icon_theme_1["upload"],
                        font_size=0,
                        mode=1)
    
    download_btn = Button([win_size.x / 20 * 19 + win_size.x / 200, 25 + win_size.x / 3 + upload_btn.size.y],
                          [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                          win_size,
                          text="download",
                          picture=icon_theme_1["download"],
                          font_size=0,
                          mode=1)

    chat_counter = Label([win_size.x + win_size.x / 200 - win_size.x / 75 - win_size.x / 400, 25 + win_size.x / 200 * 2.5 + description_btn.size.y + code_viewer_btn.size.y],
                         [win_size.x / 150, win_size.x / 150 * 4 / 3],
                         text="0",
                         text_pos="M",
                         text_v_pos="M",
                         color=Color.RED,
                         font_size=16)
    chat_counter.text.color = color_theme_0
    
    message = TextEdit([win_size.x / 2 + 25, 50 + win_size.y - win_size.y / 5],
                       [win_size.x / 2 - 130, (win_size.y / 2 - win_size.y / 50) / 4],
                       win_size,
                       text="",
                       reserv_text="Напишите сообщение:",
                       color=color_theme_1,
                       font_size=16)

    mes_btn = Button([win_size.x - 85, 50 + win_size.y - win_size.y / 5],
                          [win_size.x / 20 - win_size.x / 100, win_size.x / 20 - win_size.x / 100],
                          win_size,
                          text="send",
                          picture="mes_send.png",
                          font_size=0,
                          mode=1)
    
    r, g, b = color_theme_0.r, color_theme_0.g, color_theme_0.b
    if r > 230: r = 230
    if g > 230: g = 230
    if b > 230: b = 230
    menu_color = Color(r + 25, g + 25, b + 25)
                        
    menu = Menu([0, 0], win_size)
    menu.add_graph("Пользователь", [200, 25], text=True, color=menu_color)
    menu.add_field("Пользователь", "рейтинг", text=True, color=menu_color)
    menu.add_field("Пользователь", "выход", text=True, color=menu_color)
    menu.add_graph("Предметы", [200, 25], text=True, color=menu_color)
    menu.add_field("Предметы", "Информатика", text=True, color=menu_color)
    menu.add_field("Предметы", "Математика", text=True, color=menu_color)
    menu.add_field("Предметы", "Русский язык", text=True, color=menu_color)
    menu.add_graph("Настройки", [200, 25], text=True, color=menu_color)
    menu.add_field("Настройки", "Меню настроек", text=True, color=menu_color)
    menu.add_field("Настройки", "Смена темы", text=True, color=menu_color)
    menu.add_graph("Помощь", [200, 25], text=True, color=menu_color)
    menu.add_field("Помощь", "Справка", text=True, color=menu_color)
    menu.add_field("Помощь", "О программе", text=True, color=menu_color)

    excercises = ScrolledMenu([win_size.x / 10 + 3, 25],
                              [win_size.x / 2 - win_size.x / 10 - 3, win_size.y - 25],
                              win_size,
                              title="Задания",
                              color=color_theme_0)
    excercises_list = []
    available_excercise = ''
    available_task = ''
    available_task_num = 0

    block_3 = [description, code_viewer, chat_viewer]
    block_3_element = block_3[0]
    chat_active = False
    
    activable_elements = [excercises, menu, message]
    b_3_buttons = [description_btn, chat_viewer_btn, code_viewer_btn]
    buttons = [download_btn, upload_btn, mes_btn]
    static_elements = [avatar, username, class_num, chat_counter]
    
    window.framerate_limit = 60
    icon = Image.from_file("pic/icon.png")
    icon_path = os.getcwd() + "/pic/icon.png"
    window.set_icon(1920, 1920, bytes(icon.pixels))
    
    while window.is_open:
        window.clear(color_theme_1)
        
        events = []
        for i in window.events:
            events.append(i)
            
        for event in events:
            if event.type == Event.CLOSED:
                window.close()
                
        interface(window, win_size, color_theme_0, menu_color)
        for i in buttons:
            i.draw(window, events)
        for i in b_3_buttons:
            i.draw(window, events)
        for i in static_elements:
            i.draw(window, events)
        block_3_element.draw(window, events)
            
        if window.has_focus():
            for i in b_3_buttons:
                if i.get_focus(window, events):
                    if i.get_text() == "chat_viewer":
                        chat_active = True
                    else:
                        chat_active = False
                    block_3_element = locals()[i.get_text()]
            if chat_active:
                if chat_viewer.get_focus(window, events):
                    res = chat_viewer.focus_event(window, events)
            for i in buttons:
                if i.get_focus(window, events):
                    if i.get_text() == "upload":
                        if block_3.index(block_3_element) in [0, 1] and account["type"] == "student" and available_excercise and available_task:
                            get = excercises_list[available_task_num]["file"].split("&")
                            get_1 = [int(i.split("_")[0]) for i in get]
                            get_2 = [i.split("_")[1] for i in get]
                            get = [get_1, get_2]
                            file = fm.get_file(get)
                            print(file)
                            path = ""
                            if file and len(file) == 1:
                                try:
                                    os.mkdir(dir_path + '/tmp/')
                                except FileExistsError:
                                    pass
                                os.system(f"cp -f '{file[0]}' '{dir_path + '/tmp/'}'")
                                ext = file[0].split('/')[-1].split('.')[-1]
                                path = f"{dir_path + '/tmp/' + available_excercise + '_' + translit('_'.join(available_task.split()), 'ru', reversed=True) + '_' + account['class'] + '_' + translit(class_num.text.string.split()[2], 'ru', reversed=True) + '_' + '_'.join(username.text.string.split()) + '.' + ext}"
                                os.system(f"mv -f '{dir_path + '/tmp/' + file[0].split('/')[-1]}' '{path}'")
                            elif file and len(file) > 1:
                                try:
                                    os.mkdir(dir_path + '/tmp/')
                                except FileExistsError:
                                    pass
                                orig_path = os.getcwd()
                                path = dir_path + '/tmp/' + available_excercise + '_' + translit('_'.join(available_task.split()), 'ru', reversed=True) + '_' + account['class'] + '_' + translit(class_num.text.string.split()[2], 'ru', reversed=True) + '_' + '_'.join(username.text.string.split()) + '.zip'
                                with zipfile.ZipFile(path, 'w') as myzip:
                                    for i in file:
                                        os.chdir("/".join(i.split("/")[:-1]))
                                        myzip.write(i.split("/")[-1])
                                        os.chdir(orig_path)
                            if path:
                                with open(path, 'rb') as fobj:
                                    ftp.cwd('~')
                                    ftp.cwd(available_excercise)
                                    ftp.cwd(translit(class_num.text.string.split()[2], 'ru', reversed=True))
                                    data = ftp.retrlines('LIST')
                                    print(data, path.split('/')[-1])
                                    ftp.storbinary('STOR ' + path.split('/')[-1], fobj)
                        elif block_3.index(block_3_element) in [0, 1] and account["type"] == "student" and not available_excercise:
                            notify(title, "Ошибка", "Вы не выбрали предмет", icon_path)
                        elif block_3.index(block_3_element) in [0, 1] and account["type"] == "student" and not available_task:
                            notify(title, "Ошибка", "Вы не выбрали задание", icon_path)
                        else:
                            notify(title, "Ошибка", "Вы не можете здесь загружать файл", icon_path)
                    if i.get_text() == "send":
                        if sock_send:
                            sender(sock_send, ["teacher_{}_{}_{}".format(available_excercise, str(account["class"]), str(available_task_num + 1))], message.get_text())
                            chat_viewer.add_field(message.get_text(), text_pos="R", obj_type="button", color=color_theme_1)
                            message.set_text('')
                            chat_viewer.set_value(100)
                        else:
                            notify(title, "Ошибка", "Не удалось подключиться к серверу сообщений", os.getcwd() + "/pic/icon.png")
            elem = get_focus(activable_elements, window, events)
            if elem:
                res = elem.focus_event(window, events)
                if str(type(elem)).split(".")[1].split("'")[0] == "Menu":
                    if menu.active_fields[1] == "Предметы" and res:
                        try:
                            orig_res = res
                            res = translit(res, 'ru', reversed=True).lower()
                            print(dir_path + "/" + res + "_{}_{}".format(class_num.text.string.split()[1], translit(class_num.text.string.split()[2], 'ru', reversed=True)) + ".json")
                            excercises_list = json.load(open(dir_path + "/" + res + "_{}_{}".format(class_num.text.string.split()[1], translit(class_num.text.string.split()[2], 'ru', reversed=True)) + ".json"))
                            excercises.clear()
                            available_excercise = res
                            available_task = ''
                            available_task_num = 0
                            for excercise in excercises_list:
                                excercises.add_field(excercise["title"], text=True, color=menu_color)
                        except FileNotFoundError:
                            notify(title, "Ошибка", f"Данный предмет не доступен: {orig_res}", icon_path)
                    elif menu.active_fields[1] == "Настройки" and res == "Смена темы":
                        icon_theme_0, icon_theme_1 = icon_theme_1, icon_theme_0
                        color_theme_0, color_theme_1 = color_theme_1, color_theme_0
                        r, g, b = color_theme_0.r, color_theme_0.g, color_theme_0.b
                        if r > 230: r = 230
                        if g > 230: g = 230
                        if b > 230: b = 230
                        menu_color = Color(r + 25, g + 25, b + 25)
                        
                        menu.set_color(menu_color)
                        chat_counter.text.color = color_theme_0
                        description.set_color(color_theme_1)
                        code_viewer.set_color(color_theme_1)
                        chat_viewer.set_color(color_theme_1, menu_color)
                        username.set_color(color_theme_1)
                        class_num.set_color(color_theme_1)
                        message.set_color(color_theme_1)
                        excercises.set_color(color_theme_0, menu_color)

                        description_btn.set_picture(icon_theme_1["description"])
                        code_viewer_btn.set_picture(icon_theme_1["code_viewer"])
                        chat_viewer_btn.set_picture(icon_theme_1["chat_viewer"])
                        download_btn.set_picture(icon_theme_1["download"])
                        upload_btn.set_picture(icon_theme_1["upload"])
                    elif menu.active_fields[1] == "Пользователь" and res == "выход":
                        window.close()
                    elif res:
                        notify(title, title, res, icon_path)
                elif str(type(elem)).split(".")[1].split("'")[0] == "ScrolledMenu" and res:
                    for k, excercise in enumerate(excercises_list):
                        if excercise["title"] == res:
                            description.set_text(excercise["description"])
                            block_3_element = block_3[0]
                            available_task = excercise["title"]
                            available_task_num = k

                            #***********#
                            # messanger #
                            #***********#
                            chat_viewer.clear()
                            if account["type"] == "student":
                                try:
                                    for i in open(his_path + "chat_teacher_{}_{}_{}.hs".format(available_excercise, str(account["class"]), str(available_task_num + 1))).readlines():
                                        if ":&" in i:
                                            if i.startswith("$IAM$"):
                                                chat_viewer.add_field(i[i.find(":&") + 2:], key=i, text_pos="R", obj_type="button", color=color_theme_1)
                                            else:
                                                chat_viewer.add_field(i[i.find(":&") + 2:], key=i, text_pos="L", color=color_theme_1)
                                    chat_viewer.set_value(100)
                                    chat_viewer.draw(window, events)
                                except FileNotFoundError as e:
                                    print(e)
                            #***********#
                            break
                elif res:
                    notify(title, title, res, icon_path)
        else:
            for i in activable_elements:
                i.draw(window, events)
            for i in buttons:
                i.draw(window, events)
            for i in b_3_buttons:
                i.draw(window, events)
            for i in static_elements:
                i.draw(window, events)
                
        window.display()
    return 0

if __name__ == "__main__":

    dir_path = "/home/" + os.getenv("USER") + "/.СистемаЗачётов"
    his_path = "/home/" + os.getenv("USER") + "/.СистемаЗачётов/.history/"
    
    try:
        os.mkdir(dir_path)
    except FileExistsError:
        pass
    account = ""
    print("The programm returned:", main({'type': 'student', 'class': '10', 'login': 'zaz965@stm32f0.ru'}))
    # print("The programm returned:", init())