from ctypes import string_at
from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfml_label import Label
from sfml_notify import notify
from char_dict import get_char
import platform
import pyperclip
import os

class TextEdit(Label):
    def __init__(self, pos, size, win_size, text="", text_pos="L", text_v_pos="U", reserv_text="", color=Color.WHITE, picture="", font_size=-1, key="ESC", type=""):
        '''Initialize object TextEdit'''
        super().__init__(pos, size, text, text_pos, text_v_pos, color, picture, font_size=font_size)
        size_x, size_y = size
        self.interrupt_key = key
        self.editing = False
        self.main_text = self.text.string
        self.win_size = win_size
        self.reserv_text = reserv_text
        self.type = type

    def set_size(self, x, y):
        '''Return the object Button'''
        return TextEdit(list(self.position), (x, y), self.text.string, self.color, self.picture, self.font_size)

    def set_text(self, string):
        self.text.string = string
        if list(self.text.local_bounds)[2] > self.size_main[0]:
            if self.text.string[-3] in self.alphas:
                self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
            else:
                k = 4
                while self.text.string[-k] not in self.alphas:
                    k += 1
                k -= 1
                self.text.string = self.text.string[:-k] + "-\n" + self.text.string[-k:]
        self.main_text = string

    def get_text(self):
        return self.main_text

    def draw(self, window, events):
        window.draw(self)
        if self.font_size != 0:
            if len(self.text.string):
                window.draw(self.text)
            else:
                font = Font.from_file("fonts/NotoSerifCJK-Regular.ttc")
                t = Text(self.reserv_text, font, self.font_size)
                t.position = self.main_position
                r, g, b = self.text.color.r, self.text.color.g, self.text.color.b
                if r > 128: r = 0
                if g > 128: g = 0
                if b > 128: b = 0
                color = Color(r + 100, g + 100, b + 100)
                t.color = color
                window.draw(t)

    def get_focus(self, window, events):
        self.edit_text_event(window, events)
        self.draw(window, events)
        return self.editing        
    
    def edit_text_event(self, window, events):
        mouse_x, mouse_y = Mouse.get_position(window)
        mouse_x = mouse_x / list(window.size)[0] * list(self.win_size)[0]
        mouse_y = mouse_y / list(window.size)[1] * list(self.win_size)[1]
        pos_x, pos_y = self.position
        size_x, size_y = self.size_main
        if Mouse.is_button_pressed(0):
            if pos_x < mouse_x < pos_x + size_x and pos_y < mouse_y < pos_y + size_y:
                self.editing = True
            else:
                self.editing = False
                
    def focus_event(self, window, events):
        for event in events:
            if event.type == Event.KEY_PRESSED:
                key = get_char(dict(event.items()))
                key = key if key else "NONE"
                if Keyboard.is_key_pressed(Keyboard.L_CONTROL) and Keyboard.is_key_pressed(Keyboard.V):
                    self.main_text += pyperclip.paste()
                    self.text.string += pyperclip.paste()
                elif key.islower() or len(key) == 1:
                    self.text.string += key
                    self.main_text += key
                elif key == "BACKSPACE":
                    if len(self.text.string) != len(self.main_text):
                        self.main_text = self.main_text[:-1]
                    else:
                        self.text.string = self.text.string[:-1]
                        self.main_text = self.main_text[:-1]
                elif key == "SPACE":
                    self.text.string += " "
                    self.main_text += " "
                elif key == "ENTER":
                    self.text.string += "\n"
                    self.main_text += "\n"
                elif key == self.interrupt_key:
                    self.editing = False
                text_settings = list(self.text.local_bounds)
                if text_settings[2] > self.size_main[0] and self.text.character_size > 14:
                    self.text.character_size -= 2
                    self.text.position = self.main_position
                    while list(self.text.local_bounds)[2] > self.size_main[0]:
                        self.text.character_size -= 2
                        self.text.position = self.main_position
                        if self.text.character_size <= 14:
                            if self.text.character_size % 2 == 0:
                                self.text.character_size = 14
                            else:
                                self.text.character_size = 13
                            self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
                            self.main_text = self.text.string
                            break
                    while list(self.text.local_bounds)[3] > self.size_main[1]:
                        self.text.character_size -= 2
                        self.text.position = self.main_position
                        if self.text.character_size <= 14:
                            if self.text.character_size % 2 == 0:
                                self.text.character_size = 14
                            else:
                                self.text.character_size = 13
                            if list(self.text.local_bounds)[3] > self.size_main[1] and self.text.string == self.main_text:
                                self.text.string = self.text.string[:-3]
                            break
                elif text_settings[2] > self.size_main[0]:
                    self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
                    self.main_text = self.text.string
                    if list(self.text.local_bounds)[3] > self.size_main[1]:
                        self.text.string = self.text.string[:-3]
                elif text_settings[3] > self.size_main[1]:
                    while list(self.text.local_bounds)[3] > self.size_main[1]:
                        self.text.character_size -= 2
                        self.text.position = self.main_position
                        if self.text.character_size <= 14:
                            if self.text.character_size % 2 == 0:
                                self.text.character_size = 14
                            else:
                                self.text.character_size = 13
                            if list(self.text.local_bounds)[3] > self.size_main[1] and self.text.string == self.main_text:
                                self.text.string = self.text.string[:-3]
                            break
                    if list(self.text.local_bounds)[3] > self.size_main[1]:
                        self.text.string = self.text.string[:-3]
                elif text_settings[2] < self.size_main[0] and self.text.character_size + 2 <= self.font_size:
                    self.text.character_size += 2
                    self.text.position = self.main_position
                    if list(self.text.local_bounds)[2] > self.size_main[0] or list(self.text.local_bounds)[3] > self.size_main[1]:
                        self.text.character_size -= 2
                        self.text.position = self.main_position
                else:
                    pass
                if self.type == "password":
                    self.text.string = "*" * len(self.main_text)
                else:
                    self.text.string = self.main_text
