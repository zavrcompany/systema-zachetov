from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfml_label import Label
import os
import time

class Button(Label):
    def __init__(self, pos, size, win_size, text="", text_pos="M", text_v_pos="M", color=Color.WHITE, picture="", font_size=-1, mode=0):
        '''Initialize object Button'''
        self.win_size = win_size
        self.mode = mode
        self.activated = False
        super().__init__(pos, size, text, text_pos, text_v_pos, color, picture, font_size)

    def set_size(self, x, y):
        '''Return the object Button'''
        return Button(list(self.position), (x, y), self.win_size, self.text.string, self.text_pos, self.text_v_pos, self.color, self.picture, self.font_size)
    
    def get_focus(self, window, events):
        '''Return the button's state'''
        mouse_x, mouse_y = Mouse.get_position(window)
        mouse_x = mouse_x / list(window.size)[0] * list(self.win_size)[0]
        mouse_y = mouse_y / list(window.size)[1] * list(self.win_size)[1]
        pos_x, pos_y = list(self.position)[:]
        size_x, size_y = self.size_main
        if self.mode == 0:
            if Mouse.is_button_pressed(0):
                if pos_x < mouse_x < pos_x + size_x and pos_y < mouse_y < pos_y + size_y:
                    self.size = (size_x - self.rel_x, size_y - self.rel_y)
                    self.position = self.rel_position
                    r, g, b = self.color.r, self.color.g, self.color.b
                    if r < 50:
                        r = 50
                    if g < 50:
                        g = 50
                    if b < 50:
                        b = 50
                    self.fill_color = Color(r - 50, g - 50, b - 50)
                    return True
                else:
                    self.size = self.size_main
                    self.position = self.main_position
                    self.fill_color = self.color
            else:
                self.size = self.size_main
                self.position = self.main_position
                self.fill_color = self.color
            return False
        elif self.mode == 1:
            for i in events:
                if i.type == Event.MOUSE_BUTTON_PRESSED:
                    if pos_x < mouse_x < pos_x + size_x and pos_y < mouse_y < pos_y + size_y:
                        self.size = (size_x - self.rel_x, size_y - self.rel_y)
                        self.position = self.rel_position
                        r, g, b = self.color.r, self.color.g, self.color.b
                        if r < 50:
                            r = 50
                        if g < 50:
                            g = 50
                        if b < 50:
                            b = 50
                        self.fill_color = Color(r - 50, g - 50, b - 50)
                        self.activated = True
                if i.type == Event.MOUSE_BUTTON_RELEASED:
                    if self.activated:
                        self.size = self.size_main
                        self.position = self.main_position
                        self.fill_color = self.color
                        self.activated = False
                        return True
            return False
