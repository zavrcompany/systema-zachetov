from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfml_slider import SliderBar
from sfml_label import Label
from sfml_button import Button
import os

class ScrolledMenu:
    def __init__(self, pos, size, win_size, title="", step=0, text_pos="M", text_v_pos="M", color=Color.WHITE):
        self.pos = pos
        self.size = size
        self.win_size = win_size
        self.title = title
        self.color = color
        self.step = step
        self.start_num = 0
        
        self.text_pos = text_pos
        self.text_v_pos = text_v_pos
        self.menu = {}
        #*******
        r, g, b = self.color.r, self.color.g, self.color.b
        if r > 128: r = 0
        if g > 128: g = 0
        if b > 128: b = 0
        self.slider_color = Color(r + 100, g + 100, b + 100)
        #*******
        if step:
            self.elements = {"slider": SliderBar([pos[0] + size[0] * 0.98, pos[1] + size[1] * 0.04], [size[0] * 0.02, size[1] * 0.96], win_size, step, color=self.slider_color)}
        else:
            self.step = size[1] * 0.96 / 15
            self.elements = {"slider": SliderBar([pos[0] + size[0] * 0.98, pos[1] + size[1] * 0.04], [size[0] * 0.02, size[1] * 0.96], win_size, size[1] * 0.96 / 15, color=self.slider_color)}
        self.set_title(title)

    def add_field(self, name, text=False, color=Color.WHITE, picture=""):
        pos = self.pos[0], self.pos[1] + self.size[1] * 0.04
        step = self.elements["slider"].step
        size = self.size[0] * 0.98, self.elements["slider"].step * 0.95
        try:
            if self.menu[list(self.menu.keys())[-1]]["label"]:
                x, y = self.menu[list(self.menu.keys())[-1]]["label"].get_position()[0], self.menu[list(self.menu.keys())[-1]]["label"].get_position()[1] + step
            else:
                x, y = self.menu[list(self.menu.keys())[-1]]["btn"].get_position()[0], self.menu[list(self.menu.keys())[-1]]["btn"].get_position()[1] + step
        except IndexError:
            x, y = pos[0], pos[1]
        pos = (x, y)
        if text:
            title = name
            label = Label(pos, [size[1], size[1]], picture=picture, color=color)
            self.menu[name] = {"btn": Button([pos[0] + label.size.x, pos[1]], [size[0] - label.size.x, size[1]], self.win_size, text=title, color=color, text_pos=self.text_pos, text_v_pos=self.text_v_pos, mode=0), "name": name, "label": label, "is_hide": False}
        else:
            title = ""
            label = ""
            self.menu[name] = {"btn": Button(pos, size, self.win_size, text=title, color=color, picture=picture, text_pos=self.text_pos, text_v_pos=self.text_v_pos, mode=0), "name": name, "label": label, "is_hide": False}
        if not self.pos[1] < y < self.pos[1] + self.size[1]:
            self.menu[name]["is_hide"] = True
        if self.size[1] * 0.96 / step < len(self.menu):
            self.elements["slider"].slider = self.elements["slider"].slider.set_size(self.elements["slider"].slider.size.x,
                                                                                     self.size[1] * 0.96 * (self.size[1] * 0.96 / step) / len(self.menu))

    def clear(self):
        self.menu = {}
        self.start_num = 0
        self.elements["slider"] = SliderBar([self.pos[0] + self.size[0] * 0.98, self.pos[1] + self.size[1] * 0.04], [self.size[0] * 0.02, self.size[1] * 0.96], self.win_size, self.step, color=self.slider_color)
            
    def set_title(self, title):
        self.elements["title"] = Label(self.pos, [self.size[0], self.size[1] * 0.04], text=title, color=self.color, text_pos=self.text_pos, text_v_pos=self.text_v_pos)

    def get_focus(self, window, events):
        self.draw(window, events)
        active = []
        for i in self.menu.values():
            if not i["is_hide"]:
                active.append(i["btn"].get_focus(window, events))
        active.append(self.elements["slider"].get_focus(window, events))
        return any(active)
    
    def draw(self, window, events):
        for i in self.menu.values():
            if not i["is_hide"]:
                i["btn"].draw(window, events)
                if i["label"]:
                    i["label"].draw(window, events)
        for i in self.elements.values():
            i.draw(window, events)

    def focus_event(self, window, events):
        step = self.elements["slider"].step
        
        if self.elements["slider"].get_focus(window, events):
            self.elements["slider"].focus_event(window, events)
            if self.start_num != self.elements["slider"].value:
                rel = self.start_num - self.elements["slider"].value
                self.start_num = self.elements["slider"].value
                for i in self.menu.values():
                    x, y = list(i["btn"].get_position())[:]
                    y += step * rel
                    i["btn"].set_position(x, y)
                    i["label"].set_position(x - i["label"].size.x, y)
                    if not  self.pos[1] < y < self.pos[1] + self.size[1]:
                        i["is_hide"] = True
                    else:
                        i["is_hide"] = False

        for key, i in self.menu.items():
            if i["btn"].get_focus(window, events):
                return i["name"]

    def set_color(self, color, menu_color):
        self.color = color

        for i in self.menu.values():
            i["btn"].set_color(menu_color)
            if i["label"]:
                    i["label"].set_color(menu_color)

        r, g, b = color.r, color.g, color.b
        if r > 128: r = 0
        if g > 128: g = 0
        if b > 128: b = 0
        self.slider_color = Color(r + 100, g + 100, b + 100)
        
        self.elements["slider"].set_color(self.slider_color)
        self.elements["title"].set_color(self.color)
