import os
k = 0
s = 0
print("Сколько строк я написал:")
for i in os.listdir():
    if os.path.isfile(i):
        if i.endswith(".py"):
            sp = open(i).readlines()
            print(i + ": " + str(len(sp)))
            k += len(sp)
            for i in sp:
                s += len(i)
print("--------------------")
print("Итого:")
print("Строк:", k)
print("Символов:", s)
