import platform
import os
def notify(app, title, message, icon=0):
    system = platform.system()
    if system == "Darwin":
        command = f'''osascript -e 'display notification "{message}" with title "{title}"'''
    elif system == "Linux":
        command = f'''notify-send -t 5000 "{title}" "{message}" -a "{app}" -i "{icon}"'''
    else:
        return
    os.system(command)
