import socket
import threading
import time
import sys

nick = input("Введите свой никнейм: ")
s_nick = "send" + "&" + nick
r_nick = "recv" + "&" + nick

def reciver(sock_recv):
    while True:
        try:
            data = sock_recv.recv(1024)
            if data.decode() == "ping@!#":
                pass
            else:
                print(data.decode())
        except OSError as e:
            sys.exit("OK")
    
sock_recv = socket.socket()
sock_recv.connect(('alexavr.ru', 25545))
sock_recv.send(r_nick.encode())
threading.Thread(target=reciver, args=(sock_recv,)).start()

sock_send = socket.socket()
sock_send.connect(('alexavr.ru', 25545))
sock_send.send(s_nick.encode())

data = sock_send.recv(1024)
if data.decode() == "Ok":
    for i in range(128):
        sock_send.send(f"hello {i}".encode())
        time.sleep(0.25)
else:
    print(data.decode())

sock_send.close()
sock_recv.close()
sys.exit("OK")
