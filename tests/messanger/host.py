import socket
import threading
import time

sock = socket.socket()
sock.bind(('', 9092))
sock.listen(1)

connections = []

def sender(sender, recv, mes):
    try:
        connections[list(map(lambda x: x[0], connections)).index(recv)][1][1].sendall(f"{sender}:%:{mes}".encode())
    except BrokenPipeError as e:
        print(e)

def reciver(conn, name):
    while True:
        data = conn.recv(1024)
        if not data:
            continue
        
##        print(name, ": ", data.decode(), sep='')
        for i in list(map(lambda x: x[0], connections)):
            if i != name:
                sender(name, i, data.decode())
        

def ping(con, name):
    while True:
        try:
            con.sendall(b"ping@!#")
            time.sleep(0.5)
        except ConnectionResetError:
            try:
                connections.pop(list(map(lambda x: x[0], connections)).index(name))
            except ValueError:
                pass
        except BrokenPipeError:
            try:
                connections.pop(list(map(lambda x: x[0], connections)).index(name))
            except ValueError:
                pass
while True:
    conn, addr = sock.accept()
    data = conn.recv(1024)
    if data.decode().split("&")[1] in map(lambda x: x[0], connections) and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
        conn.sendall(b"Error, this nickname is busy")
    else:
        if data.decode().split("&")[1] not in map(lambda x: x[0], connections):
            connections.append([data.decode().split("&")[1], [0, 0]])
        if data.decode().split("&")[0] == "send" and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0]:
            connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] = conn
            threading.Thread(target=reciver, args=(conn, data.decode().split("&")[1])).start()
        elif data.decode().split("&")[0] == "recv" and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
            connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1] = conn
            threading.Thread(target=ping, args=(conn, data.decode().split("&")[1])).start()
        conn.sendall(b"Ok")

conn.close()
