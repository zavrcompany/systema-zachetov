from sfml.graphics import *
from sfml.system import *
from sfml.window import *
import os

class Label(RectangleShape):
    def __init__(self, pos, size, text="", text_pos="M", text_v_pos="M", color=Color.WHITE, picture="", font_size=-1):
        '''Initialize object Label'''
        super().__init__(Vector2(*size))

        if text_pos == "L": self.text_pos = "L"
        elif text_pos == "R": self.text_pos = "R"
        elif text_pos == "M": self.text_pos = "M"
        else: raise KeyError("Choose from (L (Left), M (Middle), R (Right))")

        if text_v_pos == "U": self.text_v_pos = "U"
        elif text_v_pos == "D": self.text_v_pos = "D"
        elif text_v_pos == "M": self.text_v_pos = "M"
        else: raise KeyError("Choose from (U (Up), M (Middle), D (Down))")
        
        self.position = pos
        self.main_position = pos
        size_x, size_y = size
        self.rel_x = size_x * 0.015
        self.rel_y = size_y * 0.015
        self.rel_position = (list(self.position)[0] + self.rel_x / 2, list(self.position)[1] + self.rel_y / 2)
        self.size_main = size
        
        self.alphas = ["a", "e", "u", "i", "o", "а", "е", "у", "о", "ё", "и", "я", "ы", "э", "ю", " "]
        
        if os.path.isfile("pic/" + picture):
            texture = Texture.from_file("pic/" + picture)
            self.texture = texture
            self.picture = picture
            self.texture.smooth = True
        else:
            self.fill_color = color
            self.picture = picture
        self.color = self.fill_color

        font = Font.from_file("fonts/NotoSansCJK-Regular.ttc")
        if font_size == -1:
            self.text = Text(text, font, size_y / 2)
            self.font_size = size_y / 2
            if list(self.text.local_bounds)[2] > self.size_main[0]:
                if self.text.string[-3] in self.alphas:
                    self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
                else:
                    k = 4
                    while self.text.string[-k] not in self.alphas:
                        k += 1
                    k -= 1
                    self.text.string = self.text.string[:-k] + "-\n" + self.text.string[-k:]
        else:
            self.text = Text(text, font, font_size)
            self.font_size = font_size
            if list(self.text.local_bounds)[2] > self.size_main[0]:
                if self.text.string[-3] in self.alphas:
                    self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
                else:
                    k = 4
                    while self.text.string[-k] not in self.alphas:
                        k += 1
                    k -= 1
                    self.text.string = self.text.string[:-k] + "-\n" + self.text.string[-k:]
        r, g, b = self.color.r, self.color.g, self.color.b
        self.text.color = Color(255 - r, 255 - g, 255 - b)
        if text_pos == "M":
            x_pos = pos[0] + size_x / 2 - list(self.text.local_bounds)[2] / 2
        elif text_pos == "L":
            x_pos = pos[0]
        elif text_pos == "R":
            x_pos = pos[0] + size_x - list(self.text.local_bounds)[2] # experimental function

        if text_v_pos == "M":
            y_pos = pos[1] + size_y / 2 - list(self.text.local_bounds)[3] / 4 * 3
        elif text_v_pos == "U":
            y_pos = pos[1]
        elif text_v_pos == "D":
            y_pos = pos[1] + size[1] - list(self.text.local_bounds)[3]# experimental function

        self.text.position = (x_pos, y_pos)

    def set_picture(self, picture):
        '''Change the texture'''
        texture = Texture.from_file("pic/" + picture)
        self.texture = texture
        self.picture = picture
        self.texture.smooth = True

    def set_color(self, color):
        self.color = color
        self.fill_color = color
        r, g, b = self.color.r, self.color.g, self.color.b
        self.text.color = Color(255 - r, 255 - g, 255 - b)

    def set_size(self, x, y):
        '''Return the object'''
        return Label(list(self.position), (x, y), self.text.string, self.color, self.picture)

    def get_size(self):
        '''Return the size of object'''
        return self.size_main

    def set_position(self, x, y):
        '''Set position to object'''
        self.position = (x, y)
        self.main_position = self.position
        self.rel_position = (list(self.position)[0] + self.rel_x / 2, list(self.position)[1] + self.rel_y / 2)

        size_x, size_y = self.size_main
        pos = list(self.main_position)[:]
        text_pos = self.text_pos
        if self.text_pos == "M":
            x_pos = pos[0] + size_x / 2 - list(self.text.local_bounds)[2] / 2
        elif self.text_pos == "L":
            x_pos = pos[0]
        elif self.text_pos == "R":
            x_pos = pos[0] + size_x - list(self.text.local_bounds)[2] * 1.2 # experimental function

        if self.text_v_pos == "M":
            y_pos = pos[1] + size_y / 2 - list(self.text.local_bounds)[3] / 4 * 3
        elif self.text_v_pos == "U":
            y_pos = pos[1]
        elif self.text_v_pos == "D":
            y_pos = pos[1] + size_x - list(self.text.local_bounds)[3] # experimental function

        self.text.position = (x_pos, y_pos)
        
    def get_position(self):
        '''Get position of object'''
        return list(self.main_position)[:]

    def set_text(self, string):
        self.text.string = string
        if list(self.text.local_bounds)[2] > self.size_main[0]:
            if self.text.string[-3] in self.alphas:
                self.text.string = self.text.string[:-2] + "-\n" + self.text.string[-2:]
            else:
                k = 4
                while self.text.string[-k] not in self.alphas:
                    k += 1
                k -= 1
                self.text.string = self.text.string[:-k] + "-\n" + self.text.string[-k:]

    def get_text(self):
        return self.text.string

    def draw(self, window, events):
        window.draw(self)
        if self.font_size != 0:
            window.draw(self.text)
