def get_focus(elements, window, events):
    for elem in elements:
        focus = elem.get_focus(window, events)
        if focus:
            for i in elements[elements.index(elem):]:
                i.get_focus(window, events)
            return elem
