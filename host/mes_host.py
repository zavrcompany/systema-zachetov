import socket
import threading
import time

sock = socket.socket()
sock.bind(('192.168.0.107', 25545))
sock.listen(1)

connections = []

def sender(sender, recv, mes):
    try:
        connections[list(map(lambda x: x[0], connections)).index(recv)][1][1].sendall(f"{sender}:%:{mes}".encode())
    except BrokenPipeError as e:
        print(e)

def reciver(conn, name):
    while True:
        raw_data = conn.recv(1024)
        if not raw_data:
            continue
        
##        print(name, ": ", data.decode(), sep='')
        data = raw_data.decode().split("&:&")
        users = []
        if data[0].startswith("[TO:") and data[0].endswith("]"):
            users = data[0][4:-1].split(' ')
        for i in users:
            if i != name and i in list(map(lambda x: x[0], connections)):
                sender(name, i, "&:&".join(data[1:]))
        

def ping(con, name):
    flag = False
    while True:
        try:
            con.sendall(b"ping@!#")
            time.sleep(1)
        except ConnectionResetError:
            try:
                if flag:
                    connections.pop(list(map(lambda x: x[0], connections)).index(name))
                    return
                else:
                    flag = not flag
            except ValueError:
                return
        except BrokenPipeError:
            try:
                if flag:
                    connections.pop(list(map(lambda x: x[0], connections)).index(name))
                    return
                else:
                    flag = not flag
            except ValueError:
                return
while True:
    conn, addr = sock.accept()
    data = conn.recv(1024)
    if data.decode().split("&")[1] in map(lambda x: x[0], connections) and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
        conn.sendall(b"Error, this nickname is busy")
    else:
        if data.decode().split("&")[1] not in map(lambda x: x[0], connections):
            connections.append([data.decode().split("&")[1], [0, 0]])
        if data.decode().split("&")[0] == "send":# and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0]:
            connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] = conn
            threading.Thread(target=reciver, args=(conn, data.decode().split("&")[1])).start()
        elif data.decode().split("&")[0] == "recv":# and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
            connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1] = conn
            threading.Thread(target=ping, args=(conn, data.decode().split("&")[1])).start()
        conn.sendall(b"Ok")

conn.close()
