import pymysql
from check_password import check_password
import socket
import asyncio
from keygen import *
from coder import *
import datetime
from flask import *
from threading import Thread
from settingImporter import Settings

settings = Settings()
host_address = (settings.get_host(), settings.get_port())
connections = {}

app = Flask(__name__)

@app.route("/check_qrcode")
def check_qrcode():
    cur.execute("SELECT number, id FROM connecting WHERE ip = %s", request.remote_addr)
    key, person_id = cur.fetchone()
    cur.execute(f"SELECT login FROM Users WHERE id={person_id}")
    login = cur.fetchone()[0]
    k = key[:7]
    if k == request.args.get("code"):
        if k in connections:
            new_key = str(gen(int(key)))
            new_login = encoding(decoding(login, key), new_key)
            print(new_login.encode('unicode_escape'))
            cur.execute(f"UPDATE connecting SET number = {new_key} WHERE id=(SELECT id FROM Users WHERE login=%s)", login.encode('unicode_escape').decode())
            cur.execute(f"UPDATE Users SET login = %s WHERE id={person_id}", [new_login.encode('unicode_escape').decode()])
            con.commit()
            connections[k].sendall(b"ok")
            print("ok")
        else:
            connections[k].sendall(b"incorrect")
            return ""
    else:
        connections[k].sendall(b"incorrect")
    return ""


async def login(connection, recv):
    login, password = recv.split("&&")
    try:
        cur.execute("SELECT * From Users")
        cur.execute("SELECT number FROM connecting WHERE id = (SELECT id from Users WHERE login=%s)", login)
        key = cur.fetchone()[0]
    except Exception:
        print(f"[Log {datetime.datetime.now().strftime('%H:%M:%S')}]:", "Incorrect login")
        connection.sendall(b"incorrect")
        return False
    password = decoding(password, key)
    cur.execute("SELECT password FROM Users WHERE login=%s", login)
    ans = cur.fetchone()
    if check_password(ans[0], password):
        new_key = str(gen(int(key)))
        new_login = encoding(decoding(login.encode().decode('unicode_escape'), key), new_key)
        print(new_login.encode('unicode_escape'))
        cur.execute(f"UPDATE connecting SET number = {new_key} WHERE id=(SELECT id FROM Users WHERE login=%s)", login)
        cur.execute("SELECT id,class,type FROM Users WHERE login=%s", login)
        person_id, person_class, person_type = cur.fetchone()
        cur.execute(f"UPDATE Users SET login = %s WHERE id={person_id}", [new_login.encode('unicode_escape').decode()])
        con.commit()
        connection.sendall("ok".encode())
        connection.sendall(f"{person_type} {person_class}".encode())
        return True
    else:
        print(f"[Log {datetime.datetime.now().strftime('%H:%M:%S')}]:", "Incorrect password")
        connection.sendall(b"incorrect")
        return False

async def analyzer(conn):
    recv = conn.recv(1024)
    recv_s = recv.decode()
    print(f"[Log {datetime.datetime.now().strftime('%H:%M:%S')}]:", recv)
    if recv_s.startswith("com://"):
        if recv_s[len("com://"):].startswith("connect"):
            if await login(conn, recv_s[len("com://connect "):]):
                connections[conn.getpeername()[1]] = [conn]
        elif recv_s[len("com://"):].startswith("init_chat"):
            connections[int(recv_s[len("com://init_chat "):])].append(conn)
        elif recv_s[len("com://"):].startswith("tasker"):
            id = int(recv_s[len("com://tasker/"):])
            conn.sendall(tasker(id))
            

async def tasker(id):
    cur.execute(f"SELECT * FROM tasks WHERE id={id}")
    data = list(cur.fetchone())
    return "#$#".join(list(map(str, data)))


con = pymysql.connect(host='localhost',
                      user='HostSystem',
                      password='SysEvalP21!',
                      database='EvaluationSystem')

def qrcode_checker():
    app.run(host="192.168.0.107", port=25545)

##Thread(target=qrcode_checker).start()

with con:
    
    cur = con.cursor()
    try:
        s = socket.socket()
        s.bind(host_address)
        print(f"Server start on {s.getsockname()[0]}:{s.getsockname()[1]}")
        s.listen(10)
        while True:
            conn, addr = s.accept()
            asyncio.run(analyzer(conn))
    except KeyboardInterrupt:
        pass
    s.close()
