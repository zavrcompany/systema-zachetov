class Settings:
    def __init__(self, file="settings.conf"):
        self.filename = file
        self.data = open(file).readlines()
        self.settings = {}
        for i in self.data:
            if "=" in i and i[0] != "#":
                self.settings[i[:i.index("=")].strip()] = i[i.index("=") + 1:].strip()

    def create_standart(self):
        s = "#settings.conf - конфигурационный файл для проекта Система Зачётов\n\n#main host configuration\nhost=192.168.0.107\nport=25555\n\n#chat configuration\nchat_host=192.168.0.107\nchat_port=25545\n\n#ftp configuration\nftp_host=192.168.0.107\nftp_port=21"
        with open(self.filename, "w") as f:
            f.write(s)
        
    def get_host(self):
        if "host" in self.settings:
            return self.settings["host"]
        return "127.0.0.1"
    
    def set_host(self, host):
        self.settings["host"] = host
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")
                    

    def get_port(self):
        if "port" in self.settings:
            return int(self.settings["port"])
        return 9000
    
    def set_port(self, port):
        self.settings["port"] = port
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")

    def get_chat_host(self):
        if "chat_host" in self.settings:
            return self.settings["chat_host"]
        return "127.0.0.1"
    
    def set_chat_host(self, host):
        self.settings["chat_host"] = host
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")             

    def get_chat_port(self):
        if "chat_port" in self.settings:
            return int(self.settings["chat_port"])
        return 9000
    
    def set_chat_port(self, port):
        self.settings["chat_port"] = port
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")
                    
    def get_ftp_host(self):
        if "ftp_host" in self.settings:
            return self.settings["ftp_host"]
        return "127.0.0.1"
    
    def set_ftp_host(self, host):
        self.settings["ftp_host"] = host
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")  

    def get_ftp_port(self):
        if "ftp_port" in self.settings:
            return int(self.settings["ftp_port"])
        return 9000
    
    def set_ftp_port(self, port):
        self.settings["ftp_port"] = port
        with open(self.filename, "w") as f:
            for i in self.data:
                if ("=" not in i or i[0] == "#" or self.settings[i[:i.index("=")].strip()] == i[i.index("=") + 1:].strip()):
                    f.write(i)
                else:
                    f.write(i[:i.index("=")] + "=" + self.settings[i[:i.index("=")]] + "\n")
