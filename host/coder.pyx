def encoding(n, k):
    k = k * (len(n) // len(k) + 1)
    crypt = ""
    for j, i in enumerate(n):
        crypt += chr(ord(i) ^ ord(k[j]))
    return crypt.encode().decode()

def decoding(n, k):
    k = k * (len(n) // len(k) + 1)
    crypt = ""
    for j, i in enumerate(n):
        crypt += chr(ord(i) ^ ord(k[j]))
    return crypt
