import socket
import threading
import time
import os
import json
from settingImporter import Settings
import asyncio

settings = Settings()

try:
    os.mkdir("messages")
except FileExistsError:
    pass

try:
    all_users = set(json.load(open("users.json")))
except Exception as e:
    print(e)
    all_users = set()
    json.dump(list(all_users), open("users.json", "w"))

sock = socket.socket()
sock.bind((settings.get_chat_host(), settings.get_chat_port()))
sock.listen(1)

connections = []

def sender(sender, recv, mes):
    try:
        connections[list(map(lambda x: x[0], connections)).index(recv)][1][1].sendall(f"{sender}:%:{mes}".encode())
    except (ValueError, BrokenPipeError):
##        print(sender, recv)
        with open(f"messages/{recv}.hs", "a") as f:
            f.write(time.strftime("%H:%M:%S", time.localtime()) + ":&" + sender + ":%:" + mes + "\n")

def reciver(conn, name):
    while True:
        raw_data = conn.recv(1024)
        if not raw_data:
            continue
        
        data = raw_data.decode().split("&:&")
##        print(name, ": ", data, sep='')
        users = []
        if data[0].startswith("[TO:") and data[0].endswith("]"):
            users = data[0][4:-1].split(' ')
        for i in users:
            if i != name and i in all_users:
                sender(name, i, "&:&".join(data[1:]))
            elif i not in all_users:
                print(f"This user: <{i}> is not in base")

async def repair(conn, file):
    with open(file) as f:
        lines = f.readlines()
        for i in lines:
            a = i.find(":&")
            b = i.find(":%:")
            sender(i[a + 2:b], data.decode().split('&')[1], i[b + 3:-1])
            await asyncio.sleep(0.05)
    os.remove(file)

def ping(con, name):
    flag = False
    while True:
        try:
            con.sendall(b"ping@!#")
            time.sleep(1)
        except ConnectionResetError:
            try:
                if flag:
                    connections.pop(list(map(lambda x: x[0], connections)).index(name))
                    return
                else:
                    flag = not flag
            except ValueError:
                return
        except BrokenPipeError:
            try:
                if flag:
                    connections.pop(list(map(lambda x: x[0], connections)).index(name))
                    return
                else:
                    flag = not flag
            except ValueError:
                return

try:
    while True:
        conn, addr = sock.accept()
        data = conn.recv(1024)
        print(data)
        if data.decode().split("&")[1] in map(lambda x: x[0], connections) and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] and connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
            conn.sendall(b"Error, this nickname is busy")
        else:
            if data.decode().split("&")[1] not in map(lambda x: x[0], connections):
                connections.append([data.decode().split("&")[1], [0, 0]])
                all_users.add(data.decode().split("&")[1])
            if data.decode().split("&")[0] == "send":# and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0]:
                connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][0] = conn
                threading.Thread(target=reciver, args=(conn, data.decode().split("&")[1])).start()
                all_users.add(data.decode().split("&")[1])
                json.dump(list(all_users), open("users.json", "w"))
            elif data.decode().split("&")[0] == "recv":# and not connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1]:
                connections[list(map(lambda x: x[0], connections)).index(data.decode().split("&")[1])][1][1] = conn
                threading.Thread(target=ping, args=(conn, data.decode().split("&")[1])).start()
                all_users.add(data.decode().split("&")[1])
                json.dump(list(all_users), open("users.json", "w"))
                if os.path.isfile(f"messages/{data.decode().split('&')[1]}.hs"):
                    asyncio.run(repair(conn, f"messages/{data.decode().split('&')[1]}.hs"))
                    
            conn.sendall(b"Ok")
except KeyboardInterrupt:
    sock.close()
