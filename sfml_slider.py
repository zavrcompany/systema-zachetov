from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfml_button import Button

class SliderBar:
    def __init__(self, pos, size, win_size, step, rotation="V", color=Color.WHITE, sensity=3):
        self.pos = pos
        self.slider_pos = pos
        self.size = size
        self.win_size = win_size
        self.sensity = sensity
        if rotation == "V": self.rotation = 0
        elif rotation == "H": self.rotation = 1
        else: raise KeyError("Choose from (V (vertical), H (horizontal))")
        self.color = color
        if self.rotation:
            self.slider = Button([pos[0], pos[1]], [size[0], size[1]], win_size, color=color)
        else:
            self.slider = Button([pos[0], pos[1]], [size[0], size[1]], win_size, color=color)
        self.step = step
        self.value = 0
        self.prev_mouse_x, self.prev_mouse_y = pos
        self.focus = False

    def get_focus(self, window, events):
        self.draw(window, events)
        for i in events:
            if i.type == Event.MOUSE_WHEEL_SCROLLED:
                self.focus = True
                return True
        if not self.focus and self.slider.get_focus(window, events):
            self.focus = True
            self.prev_mouse_x, self.prev_mouse_y = Mouse.get_position(window)
        elif not self.slider.get_focus(window, events):
            self.focus = False
        return self.slider.get_focus(window, events)

    def draw(self, window, events):
        self.slider.draw(window, events)
        
    def focus_event(self, window, events):                
        mouse_x, mouse_y = Mouse.get_position(window)            
        if self.rotation:
            mouse_rel = self.prev_mouse_x - mouse_x
            if self.pos[0] < self.slider_pos[0] - mouse_rel and self.slider_pos[0] + self.slider.size.x - mouse_rel < self.pos[0] + self.size[0]:
                self.slider_pos = self.slider_pos[0] - mouse_rel, self.slider_pos[1]
                self.slider.set_position(self.slider_pos[0], self.slider_pos[1])
                self.value = (self.slider_pos[0] - self.pos[0] + self.slider.size.x) / ((self.size[0]) / 100)
        else:
            mouse_rel = 0
            for i in events:
                if i.type == Event.MOUSE_WHEEL_SCROLLED:
                    mouse_rel = dict(i.items())["delta"] * self.sensity
            if not mouse_rel:
                mouse_rel = self.prev_mouse_y - mouse_y
            if self.pos[1] < self.slider_pos[1] - mouse_rel and self.slider_pos[1] + self.slider.size.y - mouse_rel < self.pos[1] + self.size[1]:
                self.slider_pos = self.slider_pos[0], self.slider_pos[1] - mouse_rel
                self.slider.set_position(self.slider_pos[0], self.slider_pos[1])
                self.value = (self.slider_pos[1] - self.pos[1]) / (self.size[1] / 100)

        self.prev_mouse_x, self.prev_mouse_y = mouse_x, mouse_y

    def set_value(self, value):
        self.value = value
        self.slider_pos = self.slider_pos[0], self.value * (self.size[1] / 100) + self.pos[1] - self.slider.size.y
        self.slider.set_position(self.slider_pos[0], self.slider_pos[1])
        
    def set_color(self, color):
        self.slider.set_color(color)
