import socket
import threading
import time
import sys
import os

nick = input("Введите свой никнейм: ")
s_nick = "send" + "&" + nick
r_nick = "recv" + "&" + nick
dir_path = "/home/" + os.getenv("USER") + "/.СистемаЗачётов/.history/"

def reciver(sock_recv):
    while True:
        try:
            data = sock_recv.recv(1024)
            if data.decode() == "ping@!#":
                pass
            else:
                print(data.decode())
                user = data.decode().split(":%:")
                if len(user) >= 2:
                    with open(dir_path + "chat_" + user[0] + ".hs", mode="a") as f:
                        f.write(time.strftime("%H:%M:%S", time.localtime()) + ":&" + ":%:".join(data.decode().split(":%:")[1:]) + "\n")
        except OSError as e:
            sys.exit("OK")
    
sock_recv = socket.socket()
sock_recv.connect(('192.168.0.107', 25545))
sock_recv.send(r_nick.encode())
threading.Thread(target=reciver, args=(sock_recv,)).start()

sock_send = socket.socket()
sock_send.connect(('192.168.0.107', 25545))
sock_send.send(s_nick.encode())

users = ["zaz965@stm32f0.ru"]
data = sock_send.recv(1024)
if data.decode() == "Ok":
    for i in range(100):
        sock_send.send(f"[TO:{' '.join(users)}]&:&hello {i}".encode())
        time.sleep(0.3)
else:
    print(data.decode())

sock_send.close()
sock_recv.close()
sys.exit("OK")
