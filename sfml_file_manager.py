from sfml.graphics import *
from sfml.system import *
from sfml.window import *
from sfmlWidgets import *
import os

class FileManager:
    def __init__(self):
        self.window = RenderWindow(VideoMode(640, 480), "Выберите файл")
        self.window.visible = False
        self.dir_path = "/home/" + os.getenv("USER") + "/"
        self.icons_dir = "file_manager/"
        self.icons = {"py": ["py", "pyx", "pyw"],
                      "calc": ["ods", "ots", "fods", "xls", "xlsx", "xlt", "xltx", "xlsm"],
                      "impress": ["odp", "otp", "odg", "fodp", "ppt", "pptx", "ppsx", "potx", "pps", "pot"],
                      "pdf": ["pdf"],
                      "picture": ["bmp", "png", "jpg", "jpeg", "JPG", "ico", "gif", "webp"],
                      "txt": ["txt", "sh", "log", "json"],
                      "ui": ["ui", "design"],
                      "writer": ["odt", "ott", "fodt", "docx", "dotx", "xml", "doc", "dot", "rtf", "docm"],
                      "music": ["mp3", "wav", "aac", "flac"],
                      "video": ["avi", "mp4", "3gp", "mpg", "mpeg"]}
        self.types_label = Label([0, self.window.size.y - 40], [self.window.size.x - 80, 40], text="Расширения: ", color=Color(200, 200, 200), text_pos="L", font_size=12)
        self.return_btn = Button([self.window.size.x - 80, self.window.size.y - 40], [40, 40], self.window.size, picture="return.png", mode=1)
        self.ok_btn = Button([self.window.size.x - 40, self.window.size.y - 40], [40, 40], self.window.size, text="OK", picture="btn.png", font_size=24, mode=1)
        self.list_dir = ScrolledMenu([0, 0],
                                     [self.window.size.x, self.window.size.y - 40],
                                     self.window.size,
                                     title="Путь: " + self.dir_path,
                                     step=36,
                                     color=Color(200, 200, 200),
                                     text_pos="L")

    def get_file(self, types):
##        count, types
        local_count = [0] * len(types[0])

        self.types_label.set_text("Расширения: " + ", ".join(types[1]))
        
        self.window.visible = True
        run = True
        last_path = ""
        files = []
        while run:
            self.window.clear()
            events = []
            for i in self.window.events:
                events.append(i)
                if i.type == Event.CLOSED:
                    run = False
            if self.dir_path != last_path:
                self.list_dir.clear()
                self.list_dir.set_title("Путь:" + self.dir_path)
                last_path = self.dir_path
                sl = {"dir": [], "file": []}
                for i in os.listdir(self.dir_path):
                    if os.path.isdir(self.dir_path + i):
                        sl["dir"].append(i)
                    if os.path.isfile(self.dir_path + i):
                        sl["file"].append(i)
                sl["dir"].sort()
                sl["file"].sort()
                for i in sl["dir"]:
                    self.list_dir.add_field(i, text=True, color=Color(200, 200, 200), picture=self.icons_dir + "folder" + ".png")
                for i in sl["file"]:
                    file_type = ""
                    if i[0] != ".":
                        try:
                            file_type = i.split(".")[1]
                        except IndexError:
                            file_type = ""
                    else:
                        try:
                            file_type = i.split(".")[2]
                        except IndexError:
                            file_type = ""
                    if file_type in types[1] or "*" in types[1]:
                        if file_type:
                            icon = ""
                            for k, j in self.icons.items():
                                if file_type in j:
                                    icon = k
                                    break
                            if not icon:
                                icon = "other"
                        else:
                            icon = "other"
                        self.list_dir.add_field(i, text=True, color=Color(200, 200, 200), picture=self.icons_dir + icon + ".png")
            if self.window.has_focus():
                if self.list_dir.get_focus(self.window, events):
                    res = self.list_dir.focus_event(self.window, events)
                    if res:
                        if os.path.isdir(self.dir_path + res):
                            self.dir_path = self.dir_path + res + "/"
                        else:
                            file_type = res.split(".")[-1]
                            if file_type not in types[1] and "*" in types[1]:
                                file_type = "*"
                            if self.dir_path + res not in files and file_type in types[1] and local_count[types[1].index(file_type)] < types[0][types[1].index(file_type)]:
                                local_count[types[1].index(file_type)] += 1
                                files.append(self.dir_path + res)
                            elif local_count[types[1].index(file_type)] >= types[0][types[1].index(file_type)] and self.dir_path + res not in files:
                                notify("Система Зачётов", "Ошибка", "Выбрано слишком много фалов!")
                            elif self.dir_path + res in files:
                                local_count[types[1].index(file_type)] -= 1
                                files.remove(self.dir_path + res)
                            else:
                                local_count[types[1].index(file_type)] -= 1
                                files.remove(self.dir_path + res)
                            if files:
                                self.types_label.set_text("Расширения: " + ", ".join(types[1]) + " | " + "Файлы: " + ", ".join(list(map(lambda x: x.split("/")[-1], files))))
                            else:
                                self.types_label.set_text("Расширения: " + ", ".join(types[1]))
                if self.ok_btn.get_focus(self.window, events):
                    if local_count == types[0]:
                        self.window.visible = False
                        return files
                if self.return_btn.get_focus(self.window, events):
                    if self.dir_path != "/":
                        self.dir_path = "/".join(self.dir_path.split("/")[:-2])
                        self.dir_path += "/"
                self.return_btn.draw(self.window, events)
                self.ok_btn.draw(self.window, events)
            else:
                self.list_dir.draw(self.window, events)
                self.return_btn.draw(self.window, events)
                self.ok_btn.draw(self.window, events)
            self.types_label.draw(self.window, events)
            self.window.display()
        self.window.visible = False
    
if __name__ == "__main__":
    fm = FileManager()
    print(fm.get_file([[1, 1], ["py", "*"]]))
